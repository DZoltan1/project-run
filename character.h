#include <SFML/Graphics.hpp>
using namespace sf;

class Player{

    Vector2f p_pos;
    Vector2f size;
    int speed;
    double velocity;
    bool jumplock;

public:
    Player(float x, float y, float sx, float sy, int sspd);
    void make();

    bool left;
    bool right;

    void handleMovement(int dir);
    void setSpeed(int sspeed);
    Vector2f getP_pos(){return p_pos;}
    void jump();
    void gravity();
    inline bool player_fall();
    void game_mechanism();
    void movement();
};