#include <iostream>
#include <vector>
#include "character.h"


using namespace sf;

RenderWindow window(VideoMode(1024,768),"ProjectRun");
RectangleShape character;
Player player(500,500,20,20,10);
Event event ;

void show(){

    window.display();
    window.clear();
    window.draw(character);

}


int main(){

    window.setFramerateLimit(180);

    player.make();

    while(window.isOpen()){
        
        while (window.pollEvent(event))
		{
			if(event.type == Event::Closed)
			{
				window.close();
			}
            else if(event.key.code == Keyboard::Escape){window.close();}
            else if (event.key.code == Keyboard::Space) {player.jump();}
            if(event.type == Event::KeyPressed){
                if (event.key.code == Keyboard::A){ player.handleMovement(1);}
                else if (event.key.code == Keyboard::D){ player.handleMovement(2);}
                }

            else if(event.type == Event::KeyReleased){
                if (event.key.code == Keyboard::A){ player.handleMovement(4);}
                else if (event.key.code == Keyboard::D){ player.handleMovement(5);}

    }
        
        }

    player.game_mechanism();

    show();

    }
    return 0;
}