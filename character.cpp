#include "character.h"
#include <iostream>

using namespace sf;

extern RectangleShape character;
extern Event event;

void show();
void wait_for_input();

Player::Player(float x, float y, float sx, float sy, int sspd){

    p_pos.x = x;
    p_pos.y = y;
    size.x = sx;
    size.y = sy;
    speed = sspd;

}

void Player::make(){

    character.setSize(sf::Vector2f(size));
	character.setPosition(p_pos);
	character.setFillColor(Color::White);

}

void Player::handleMovement(int dir){

    switch(dir){

        case 1 : left = true; break;
        case 2 : right = true; break;
        case 3 : jump(); break;
        case 4 : left = false; break;
        case 5 : right = false; break;
    }

    movement();

}

void Player::movement(){

    if(left){
        while(velocity != -10){
            velocity-=0.5;}
        character.move(velocity*0.6, 0);
        p_pos.x += velocity*0.6;
    }
    else if (right){
        while(velocity != 10){
            velocity+=0.5;}
        character.move(velocity*0.6, 0);
        p_pos.x +=velocity*0.6;
    }
    else velocity = 0;
}


void Player::jump(){
    int height = 200 ; 
    if(player_fall() == false){
        for(int i = 0; i < height ; i+=5){
                character.move(0,-5);
                p_pos.y -= 5;
                if(Keyboard::isKeyPressed(Keyboard::A)){character.move(-3,0);p_pos.x -=3;}
                else if(Keyboard::isKeyPressed(Keyboard::D)){character.move(3,0);p_pos.x +=3;}
                show();
                std::cout << velocity << " " << p_pos.x << " " << p_pos.y<< std::endl;
            }
    }
}

void Player::gravity(){


    while( p_pos.y < 500){

            character.move(0,5);
            p_pos.y += 5;
            if(Keyboard::isKeyPressed(Keyboard::A)){character.move(-3,0);p_pos.x -=3;}
            else if(Keyboard::isKeyPressed(Keyboard::D)){character.move(3,0);p_pos.x +=3;}
            show();
            std::cout << velocity << " " << p_pos.x << " " << p_pos.y<< std::endl;  
    }

}


inline bool Player::player_fall(){

    if(p_pos.y < 500) return true;
    else return false;
}

void  Player::game_mechanism(){

    std::cout << velocity << " " << p_pos.x << " " << p_pos.y<< std::endl;
    if(player_fall()){
        gravity();}
    else if(velocity != 0) movement();

}
